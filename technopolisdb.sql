-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Лют 04 2021 р., 04:19
-- Версія сервера: 10.3.22-MariaDB
-- Версія PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `technopolisdb`
--

-- --------------------------------------------------------

--
-- Структура таблиці `order_by`
--

CREATE TABLE `order_by` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `order_info` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `order_by`
--

INSERT INTO `order_by` (`id`, `id_user`, `order_info`) VALUES
(1, 1, NULL),
(2, 2, 'Celeron G4930  4444  2  < \nСума = 8888'),
(3, 3, 'Ryzen 5 3600  5777  1  < \nRyzen 5 5600X  8050  11  < \nСума = 94327'),
(4, 4, NULL),
(5, 5, NULL),
(6, 6, 'X399 Aorus Pro  9995  1  < \nRyzen 5 3600  5777  1  < \nMPG Z490  5211  1  < \nRX 5700 XT  18000  2  < \nСума = 56983');

-- --------------------------------------------------------

--
-- Структура таблиці `producer`
--

CREATE TABLE `producer` (
  `id` int(11) NOT NULL,
  `producer_name` varchar(100) NOT NULL,
  `country` varchar(20) NOT NULL,
  `site` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `producer`
--

INSERT INTO `producer` (`id`, `producer_name`, `country`, `site`) VALUES
(1, 'GYGABYTE', 'USA', 'http:/www.GYGABYTE.com'),
(2, 'ASUS', 'USA', 'http:/www.ASUS.com'),
(3, 'INTEL', 'USA', 'http:/www.INTEL.com'),
(4, 'AMD', 'USA', 'http:/www.AMD.com'),
(5, 'MSI', 'USA', 'http:/www.MSI.com'),
(6, 'NVIDIA', 'USA', 'http:/www.NVIDIA.com');

-- --------------------------------------------------------

--
-- Структура таблиці `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `id_producer` int(11) NOT NULL,
  `id_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `id_producer`, `id_type`) VALUES
(1, 'I5 9900K', 7000, 3, 1),
(2, 'RX 5700 XT', 18000, 4, 2),
(3, 'X399 Aorus Pro', 9995, 1, 6),
(4, 'ROG Strix B450-F', 3600, 2, 4),
(5, 'MPG Z490', 5211, 5, 3),
(6, 'Quadro RTX 5000', 68006, 6, 5),
(7, 'Ryzen 5 3600', 5777, 4, 1),
(8, 'Core i3-10100F', 3021, 3, 1),
(9, 'Ryzen 5 5600X', 8050, 4, 1),
(10, 'Celeron G4930', 4444, 3, 1),
(11, 'Core i7-10700K', 7655, 3, 1),
(12, 'Core i7-10700', 4533, 3, 1),
(13, 'TUF B450-Pro', 5633, 2, 6),
(14, 'Prime H310M-R', 3553, 2, 6),
(15, 'TUF Gaming X570-Plus', 3243, 2, 6),
(16, 'H410M S2H', 3332, 1, 6),
(17, 'MPG Z490', 1323, 5, 6),
(18, 'MAG B460 Torpedo', 2344, 5, 6),
(19, 'Vega 64', 12300, 4, 2),
(20, 'Ryzen 5 1600X', 4600, 4, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `type`
--

INSERT INTO `type` (`id`, `type`) VALUES
(1, 'CPU'),
(2, 'GPU'),
(3, 'RAM'),
(4, 'SSD'),
(5, 'HDD'),
(6, 'M-BOARD'),
(7, 'BP');

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(20) CHARACTER SET utf8 NOT NULL,
  `pass` varchar(32) CHARACTER SET utf8 NOT NULL,
  `position` varchar(20) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `login`, `pass`, `position`) VALUES
(1, 'admin', '111', 'admin'),
(2, 'qwer', '111', 'client'),
(3, 'vega', '111', 'client'),
(4, 'qwerty', '111', 'client'),
(5, 'qwerty1', '111', 'client'),
(6, 'qwer1', '111', 'client');

--
-- Тригери `user`
--
DELIMITER $$
CREATE TRIGGER `order_add` AFTER INSERT ON `user` FOR EACH ROW INSERT INTO order_by VALUES (null, NEW.id, null)
$$
DELIMITER ;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `order_by`
--
ALTER TABLE `order_by`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_fk0` (`id_user`) USING BTREE;

--
-- Індекси таблиці `producer`
--
ALTER TABLE `producer`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_fk0` (`id_producer`),
  ADD KEY `product_fk1` (`id_type`);

--
-- Індекси таблиці `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD UNIQUE KEY `user_id` (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `order_by`
--
ALTER TABLE `order_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблиці `producer`
--
ALTER TABLE `producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблиці `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблиці `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `order_by`
--
ALTER TABLE `order_by`
  ADD CONSTRAINT `order_fk0` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_fk0` FOREIGN KEY (`id_producer`) REFERENCES `producer` (`id`),
  ADD CONSTRAINT `product_fk1` FOREIGN KEY (`id_type`) REFERENCES `type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
