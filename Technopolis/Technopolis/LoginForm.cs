﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Technopolis
{

    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            this.textBox_pass.AutoSize = false;
            this.textBox_pass.Size = new Size(this.textBox_pass.Size.Width, 65);
        }
        private void label_END_program_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label_END_program_MouseEnter(object sender, EventArgs e)
        {
            label_END_program.ForeColor = Color.Red;
        }

        private void label_END_program_MouseLeave(object sender, EventArgs e)
        {
            label_END_program.ForeColor = Color.FromArgb(88, 24, 69);
        }

        private void label_register_window_MouseEnter(object sender, EventArgs e)
        {
            label_register_window.ForeColor = Color.Red;
        }

        private void label_register_window_MouseLeave(object sender, EventArgs e)
        {
            label_register_window.ForeColor = Color.FromArgb(88, 24, 69);
        }

        Point lastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button==MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void button_login_Click(object sender, EventArgs e)
        {
            GlobalVars.loginUser = textBox_login.Text.ToLower();
            String passUser = textBox_pass.Text;

            DB db = new DB();

            DataTable table = new DataTable();
            DataTable table_user = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command = new MySqlCommand("SELECT * FROM `user` WHERE `login` = @uL AND `pass` = @uP", db.getConnection());
            command.Parameters.Add("@uL", MySqlDbType.VarChar).Value = GlobalVars.loginUser;
            command.Parameters.Add("@uP", MySqlDbType.VarChar).Value = passUser;

            adapter.SelectCommand = command;
            adapter.Fill(table);


            if (table.Rows.Count > 0)
            { 
                this.Hide();
                MainForm mainForm = new MainForm();
                mainForm.Show();
            }
            else
            {
                MessageBox.Show("Данні не вірні");
            }
        }

        private void label_register_window_Click(object sender, EventArgs e)
        {
            this.Hide();
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
        }

    }
    public class GlobalVars
    {
        public static String loginUser;
    }
}
