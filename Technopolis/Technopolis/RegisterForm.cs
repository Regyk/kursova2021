﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Technopolis
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
            this.textBox_pass.AutoSize = false;
            this.textBox_pass.Size = new Size(this.textBox_pass.Size.Width, 65);
            this.textBox_confirm_pass.AutoSize = false;
            this.textBox_confirm_pass.Size = new Size(this.textBox_confirm_pass.Size.Width, 65);
            textBox_login.ForeColor = Color.Gray;
        }
        private void textBox_login_Click(object sender, EventArgs e)
        {
            if(textBox_login.Text == "Введіть логін")
                        {
                            textBox_login.Text = "";
                            textBox_login.ForeColor = Color.Black;
                        }
        }
        private void label_END_program_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void label_autorisation_window_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
        }
        private void label_END_program_MouseEnter(object sender, EventArgs e)
        {
            label_END_program.ForeColor = Color.Red;
        }

        private void label_END_program_MouseLeave(object sender, EventArgs e)
        {
            label_END_program.ForeColor = Color.FromArgb(88, 24, 69);
        }

        Point lastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void label_autorisation_window_MouseEnter(object sender, EventArgs e)
        {
            label_autorisation_window.ForeColor = Color.Red;
        }

        private void label_autorisation_window_MouseLeave(object sender, EventArgs e)
        {
            label_autorisation_window.ForeColor = Color.FromArgb(88, 24, 69);
        }

        private void button_geristration_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            DataTable table = new DataTable();
            DataTable table_user = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            if (textBox_login.Text == "Введіть логін" || textBox_login.Text == "" || textBox_login.Text.Length <=3)
            {
                MessageBox.Show("Логін введено не коректно");
                return;
            }
            if (textBox_pass.Text.Length <= 2 || textBox_pass.Text != textBox_confirm_pass.Text)
            {
                MessageBox.Show("Пароль занадто простий або не підтверджений");
                return;
            }

            MySqlCommand command = new MySqlCommand("SELECT * FROM `user` WHERE `login` = @uL", db.getConnection());
            command.Parameters.Add("@uL", MySqlDbType.VarChar).Value = textBox_login.Text;

            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("користувач з таким логіном вже зареєcтрований");
                return;
            }
            else
            {
                MySqlCommand command_register = new MySqlCommand("INSERT INTO `user` (`login`, `pass`, `position`) VALUES (@login, @pass, 'client');", db.getConnection());

                command_register.Parameters.Add("@login", MySqlDbType.VarChar).Value = textBox_login.Text.ToLower();
                command_register.Parameters.Add("@pass", MySqlDbType.VarChar).Value = textBox_pass.Text;

                db.openConnection();

                if (command_register.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Реєстрація успішна!");
                    this.Hide();
                    LoginForm loginForm = new LoginForm();
                    loginForm.Show();
                }

                db.closeConnection();
            }
        }

    }
}
