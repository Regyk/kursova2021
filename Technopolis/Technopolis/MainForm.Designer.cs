﻿
namespace Technopolis
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.button_product_add = new System.Windows.Forms.Button();
            this.comboBox_produser_select = new System.Windows.Forms.ComboBox();
            this.comboBox_type_select = new System.Windows.Forms.ComboBox();
            this.button_unlogin = new System.Windows.Forms.Button();
            this.label_summ = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button_bye = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGrid_order = new System.Windows.Forms.DataGridView();
            this.label_refresh = new System.Windows.Forms.Label();
            this.dataGrid_product = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGrid_producer = new System.Windows.Forms.DataGridView();
            this.dataGrid_type = new System.Windows.Forms.DataGridView();
            this.label_END_program = new System.Windows.Forms.Label();
            this.dataGrid_users = new System.Windows.Forms.DataGridView();
            this.textBox_order = new System.Windows.Forms.TextBox();
            this.textBox_product_add = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_order)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_product)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_producer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_users)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.textBox_price);
            this.panel1.Controls.Add(this.textBox_product_add);
            this.panel1.Controls.Add(this.button_product_add);
            this.panel1.Controls.Add(this.comboBox_produser_select);
            this.panel1.Controls.Add(this.comboBox_type_select);
            this.panel1.Controls.Add(this.button_unlogin);
            this.panel1.Controls.Add(this.label_summ);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.button_bye);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dataGrid_order);
            this.panel1.Controls.Add(this.label_refresh);
            this.panel1.Controls.Add(this.dataGrid_product);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dataGrid_producer);
            this.panel1.Controls.Add(this.dataGrid_type);
            this.panel1.Controls.Add(this.label_END_program);
            this.panel1.Controls.Add(this.dataGrid_users);
            this.panel1.Controls.Add(this.textBox_order);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(900, 500);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // textBox_price
            // 
            this.textBox_price.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_price.BackColor = System.Drawing.Color.Honeydew;
            this.textBox_price.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold);
            this.textBox_price.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.textBox_price.Location = new System.Drawing.Point(381, 434);
            this.textBox_price.MaxLength = 15;
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.Size = new System.Drawing.Size(174, 42);
            this.textBox_price.TabIndex = 20;
            this.textBox_price.Visible = false;
            // 
            // button_product_add
            // 
            this.button_product_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.button_product_add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_product_add.FlatAppearance.BorderSize = 0;
            this.button_product_add.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.button_product_add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(148)))), ((int)(((byte)(60)))));
            this.button_product_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_product_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_product_add.Location = new System.Drawing.Point(812, 423);
            this.button_product_add.Name = "button_product_add";
            this.button_product_add.Size = new System.Drawing.Size(62, 61);
            this.button_product_add.TabIndex = 19;
            this.button_product_add.Text = "+";
            this.button_product_add.UseVisualStyleBackColor = false;
            this.button_product_add.Visible = false;
            this.button_product_add.Click += new System.EventHandler(this.button_product_add_Click);
            // 
            // comboBox_produser_select
            // 
            this.comboBox_produser_select.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_produser_select.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_produser_select.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.comboBox_produser_select.FormattingEnabled = true;
            this.comboBox_produser_select.Items.AddRange(new object[] {
            "GYGABYTE",
            "ASUS",
            "INTEL",
            "AMD",
            "MSI",
            "NVIDIA"});
            this.comboBox_produser_select.Location = new System.Drawing.Point(669, 440);
            this.comboBox_produser_select.Name = "comboBox_produser_select";
            this.comboBox_produser_select.Size = new System.Drawing.Size(137, 32);
            this.comboBox_produser_select.TabIndex = 18;
            this.comboBox_produser_select.Visible = false;
            // 
            // comboBox_type_select
            // 
            this.comboBox_type_select.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_type_select.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_type_select.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.comboBox_type_select.FormattingEnabled = true;
            this.comboBox_type_select.Items.AddRange(new object[] {
            "CPU",
            "GPU",
            "RAM",
            "SSD",
            "HDD",
            "M-BOARD",
            "BP"});
            this.comboBox_type_select.Location = new System.Drawing.Point(561, 440);
            this.comboBox_type_select.Name = "comboBox_type_select";
            this.comboBox_type_select.Size = new System.Drawing.Size(102, 32);
            this.comboBox_type_select.TabIndex = 17;
            this.comboBox_type_select.Visible = false;
            // 
            // button_unlogin
            // 
            this.button_unlogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.button_unlogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_unlogin.FlatAppearance.BorderSize = 0;
            this.button_unlogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.button_unlogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(148)))), ((int)(((byte)(60)))));
            this.button_unlogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_unlogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_unlogin.Location = new System.Drawing.Point(789, 3);
            this.button_unlogin.Name = "button_unlogin";
            this.button_unlogin.Size = new System.Drawing.Size(85, 49);
            this.button_unlogin.TabIndex = 15;
            this.button_unlogin.Text = "вийти з акаунту";
            this.button_unlogin.UseVisualStyleBackColor = false;
            this.button_unlogin.Click += new System.EventHandler(this.button_unlogin_Click);
            // 
            // label_summ
            // 
            this.label_summ.AutoSize = true;
            this.label_summ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.label_summ.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold);
            this.label_summ.Location = new System.Drawing.Point(662, 364);
            this.label_summ.Name = "label_summ";
            this.label_summ.Size = new System.Drawing.Size(0, 39);
            this.label_summ.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(536, 364);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 39);
            this.label4.TabIndex = 11;
            this.label4.Text = "Сума";
            // 
            // button_bye
            // 
            this.button_bye.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.button_bye.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_bye.FlatAppearance.BorderSize = 0;
            this.button_bye.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.button_bye.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(148)))), ((int)(((byte)(60)))));
            this.button_bye.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_bye.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold);
            this.button_bye.Location = new System.Drawing.Point(543, 423);
            this.button_bye.Name = "button_bye";
            this.button_bye.Size = new System.Drawing.Size(263, 65);
            this.button_bye.TabIndex = 10;
            this.button_bye.Text = "ЗАМОВИТИ";
            this.button_bye.UseVisualStyleBackColor = false;
            this.button_bye.Click += new System.EventHandler(this.button_bye_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.label3.Location = new System.Drawing.Point(555, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(223, 39);
            this.label3.TabIndex = 9;
            this.label3.Text = "Замовлення";
            // 
            // dataGrid_order
            // 
            this.dataGrid_order.AllowUserToAddRows = false;
            this.dataGrid_order.AllowUserToDeleteRows = false;
            this.dataGrid_order.AllowUserToResizeColumns = false;
            this.dataGrid_order.AllowUserToResizeRows = false;
            this.dataGrid_order.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGrid_order.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_order.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGrid_order.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGrid_order.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_order.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(247)))), ((int)(((byte)(166)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_order.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGrid_order.EnableHeadersVisualStyles = false;
            this.dataGrid_order.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_order.Location = new System.Drawing.Point(474, 74);
            this.dataGrid_order.MultiSelect = false;
            this.dataGrid_order.Name = "dataGrid_order";
            this.dataGrid_order.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_order.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGrid_order.RowHeadersVisible = false;
            this.dataGrid_order.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGrid_order.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGrid_order.Size = new System.Drawing.Size(400, 287);
            this.dataGrid_order.TabIndex = 8;
            this.dataGrid_order.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_order_CellContentClick);
            // 
            // label_refresh
            // 
            this.label_refresh.AutoSize = true;
            this.label_refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold);
            this.label_refresh.Location = new System.Drawing.Point(329, 413);
            this.label_refresh.Margin = new System.Windows.Forms.Padding(0);
            this.label_refresh.Name = "label_refresh";
            this.label_refresh.Size = new System.Drawing.Size(67, 63);
            this.label_refresh.TabIndex = 7;
            this.label_refresh.Text = "⟳";
            this.label_refresh.Click += new System.EventHandler(this.label_refresh_Click);
            this.label_refresh.MouseEnter += new System.EventHandler(this.label_refresh_MouseEnter);
            this.label_refresh.MouseLeave += new System.EventHandler(this.label_refresh_MouseLeave);
            // 
            // dataGrid_product
            // 
            this.dataGrid_product.AllowUserToAddRows = false;
            this.dataGrid_product.AllowUserToDeleteRows = false;
            this.dataGrid_product.AllowUserToResizeColumns = false;
            this.dataGrid_product.AllowUserToResizeRows = false;
            this.dataGrid_product.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGrid_product.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_product.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGrid_product.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGrid_product.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_product.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(247)))), ((int)(((byte)(166)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_product.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGrid_product.EnableHeadersVisualStyles = false;
            this.dataGrid_product.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_product.Location = new System.Drawing.Point(30, 246);
            this.dataGrid_product.MultiSelect = false;
            this.dataGrid_product.Name = "dataGrid_product";
            this.dataGrid_product.ReadOnly = true;
            this.dataGrid_product.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_product.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGrid_product.RowHeadersVisible = false;
            this.dataGrid_product.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGrid_product.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGrid_product.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGrid_product.Size = new System.Drawing.Size(296, 229);
            this.dataGrid_product.TabIndex = 6;
            this.dataGrid_product.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_product_CellContentClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.label2.Location = new System.Drawing.Point(188, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Виробник";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.label1.Location = new System.Drawing.Point(26, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Комплектуючі";
            // 
            // dataGrid_producer
            // 
            this.dataGrid_producer.AllowUserToAddRows = false;
            this.dataGrid_producer.AllowUserToDeleteRows = false;
            this.dataGrid_producer.AllowUserToResizeColumns = false;
            this.dataGrid_producer.AllowUserToResizeRows = false;
            this.dataGrid_producer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGrid_producer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.dataGrid_producer.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_producer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGrid_producer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_producer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGrid_producer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_producer.ColumnHeadersVisible = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(247)))), ((int)(((byte)(166)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_producer.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGrid_producer.EnableHeadersVisualStyles = false;
            this.dataGrid_producer.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_producer.Location = new System.Drawing.Point(176, 51);
            this.dataGrid_producer.MultiSelect = false;
            this.dataGrid_producer.Name = "dataGrid_producer";
            this.dataGrid_producer.ReadOnly = true;
            this.dataGrid_producer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_producer.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGrid_producer.RowHeadersVisible = false;
            this.dataGrid_producer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGrid_producer.Size = new System.Drawing.Size(132, 198);
            this.dataGrid_producer.TabIndex = 3;
            this.dataGrid_producer.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_producer_CellContentClick);
            // 
            // dataGrid_type
            // 
            this.dataGrid_type.AllowUserToAddRows = false;
            this.dataGrid_type.AllowUserToDeleteRows = false;
            this.dataGrid_type.AllowUserToResizeColumns = false;
            this.dataGrid_type.AllowUserToResizeRows = false;
            this.dataGrid_type.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGrid_type.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGrid_type.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_type.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGrid_type.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_type.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGrid_type.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_type.ColumnHeadersVisible = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(247)))), ((int)(((byte)(166)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_type.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGrid_type.EnableHeadersVisualStyles = false;
            this.dataGrid_type.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_type.Location = new System.Drawing.Point(30, 51);
            this.dataGrid_type.MultiSelect = false;
            this.dataGrid_type.Name = "dataGrid_type";
            this.dataGrid_type.ReadOnly = true;
            this.dataGrid_type.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_type.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGrid_type.RowHeadersVisible = false;
            this.dataGrid_type.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGrid_type.Size = new System.Drawing.Size(155, 198);
            this.dataGrid_type.TabIndex = 2;
            this.dataGrid_type.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_type_CellContentClick);
            // 
            // label_END_program
            // 
            this.label_END_program.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_END_program.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_END_program.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.label_END_program.Location = new System.Drawing.Point(877, 0);
            this.label_END_program.Name = "label_END_program";
            this.label_END_program.Size = new System.Drawing.Size(23, 26);
            this.label_END_program.TabIndex = 1;
            this.label_END_program.Text = "x";
            this.label_END_program.Click += new System.EventHandler(this.label_END_program_Click);
            this.label_END_program.MouseEnter += new System.EventHandler(this.label_END_program_MouseEnter);
            this.label_END_program.MouseLeave += new System.EventHandler(this.label_END_program_MouseLeave);
            // 
            // dataGrid_users
            // 
            this.dataGrid_users.AllowUserToAddRows = false;
            this.dataGrid_users.AllowUserToDeleteRows = false;
            this.dataGrid_users.AllowUserToResizeColumns = false;
            this.dataGrid_users.AllowUserToResizeRows = false;
            this.dataGrid_users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGrid_users.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_users.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGrid_users.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGrid_users.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_users.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGrid_users.ColumnHeadersHeight = 30;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(247)))), ((int)(((byte)(166)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_users.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGrid_users.EnableHeadersVisualStyles = false;
            this.dataGrid_users.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.dataGrid_users.Location = new System.Drawing.Point(44, 65);
            this.dataGrid_users.MultiSelect = false;
            this.dataGrid_users.Name = "dataGrid_users";
            this.dataGrid_users.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_users.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGrid_users.RowHeadersVisible = false;
            this.dataGrid_users.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGrid_users.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGrid_users.Size = new System.Drawing.Size(230, 296);
            this.dataGrid_users.TabIndex = 13;
            this.dataGrid_users.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_users_CellContentClick);
            // 
            // textBox_order
            // 
            this.textBox_order.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.textBox_order.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_order.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.textBox_order.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.textBox_order.Location = new System.Drawing.Point(508, 74);
            this.textBox_order.Multiline = true;
            this.textBox_order.Name = "textBox_order";
            this.textBox_order.ReadOnly = true;
            this.textBox_order.Size = new System.Drawing.Size(336, 329);
            this.textBox_order.TabIndex = 14;
            this.textBox_order.Visible = false;
            // 
            // textBox_product_add
            // 
            this.textBox_product_add.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_product_add.BackColor = System.Drawing.Color.Honeydew;
            this.textBox_product_add.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_product_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold);
            this.textBox_product_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.textBox_product_add.Location = new System.Drawing.Point(65, 434);
            this.textBox_product_add.MaxLength = 15;
            this.textBox_product_add.Name = "textBox_product_add";
            this.textBox_product_add.Size = new System.Drawing.Size(310, 42);
            this.textBox_product_add.TabIndex = 16;
            this.textBox_product_add.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 500);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_order)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_product)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_producer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_users)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label_END_program;
        private System.Windows.Forms.DataGridView dataGrid_type;
        private System.Windows.Forms.DataGridView dataGrid_producer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGrid_product;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_refresh;
        private System.Windows.Forms.DataGridView dataGrid_order;
        private System.Windows.Forms.Button button_bye;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_summ;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGrid_users;
        private System.Windows.Forms.TextBox textBox_order;
        private System.Windows.Forms.Button button_unlogin;
        private System.Windows.Forms.ComboBox comboBox_type_select;
        private System.Windows.Forms.TextBox textBox_product_add;
        private System.Windows.Forms.Button button_product_add;
        private System.Windows.Forms.ComboBox comboBox_produser_select;
        private System.Windows.Forms.TextBox textBox_price;
    }
}