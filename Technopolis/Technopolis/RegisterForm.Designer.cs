﻿
namespace Technopolis
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox_confirm_pass = new System.Windows.Forms.TextBox();
            this.label_autorisation_window = new System.Windows.Forms.Label();
            this.button_geristration = new System.Windows.Forms.Button();
            this.textBox_pass = new System.Windows.Forms.TextBox();
            this.pictureBox_lock = new System.Windows.Forms.PictureBox();
            this.textBox_login = new System.Windows.Forms.TextBox();
            this.label_END_program = new System.Windows.Forms.Label();
            this.pictureBox_login = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_lock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_login)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.textBox_confirm_pass);
            this.panel1.Controls.Add(this.label_autorisation_window);
            this.panel1.Controls.Add(this.button_geristration);
            this.panel1.Controls.Add(this.textBox_pass);
            this.panel1.Controls.Add(this.pictureBox_lock);
            this.panel1.Controls.Add(this.textBox_login);
            this.panel1.Controls.Add(this.label_END_program);
            this.panel1.Controls.Add(this.pictureBox_login);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(650, 400);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // textBox_confirm_pass
            // 
            this.textBox_confirm_pass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_confirm_pass.BackColor = System.Drawing.Color.Honeydew;
            this.textBox_confirm_pass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_confirm_pass.Font = new System.Drawing.Font("Unispace", 40F, System.Drawing.FontStyle.Bold);
            this.textBox_confirm_pass.Location = new System.Drawing.Point(375, 214);
            this.textBox_confirm_pass.MaxLength = 15;
            this.textBox_confirm_pass.Name = "textBox_confirm_pass";
            this.textBox_confirm_pass.Size = new System.Drawing.Size(250, 64);
            this.textBox_confirm_pass.TabIndex = 6;
            this.textBox_confirm_pass.UseSystemPasswordChar = true;
            // 
            // label_autorisation_window
            // 
            this.label_autorisation_window.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.label_autorisation_window.Font = new System.Drawing.Font("Unispace", 8F, System.Drawing.FontStyle.Bold);
            this.label_autorisation_window.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.label_autorisation_window.Location = new System.Drawing.Point(275, 378);
            this.label_autorisation_window.Name = "label_autorisation_window";
            this.label_autorisation_window.Size = new System.Drawing.Size(100, 15);
            this.label_autorisation_window.TabIndex = 1;
            this.label_autorisation_window.Text = "Авторизація";
            this.label_autorisation_window.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_autorisation_window.Click += new System.EventHandler(this.label_autorisation_window_Click);
            this.label_autorisation_window.MouseEnter += new System.EventHandler(this.label_autorisation_window_MouseEnter);
            this.label_autorisation_window.MouseLeave += new System.EventHandler(this.label_autorisation_window_MouseLeave);
            // 
            // button_geristration
            // 
            this.button_geristration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.button_geristration.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_geristration.FlatAppearance.BorderSize = 0;
            this.button_geristration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(243)))), ((int)(((byte)(0)))));
            this.button_geristration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(148)))), ((int)(((byte)(60)))));
            this.button_geristration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_geristration.Font = new System.Drawing.Font("Unispace", 27.75F, System.Drawing.FontStyle.Bold);
            this.button_geristration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.button_geristration.Location = new System.Drawing.Point(150, 312);
            this.button_geristration.Name = "button_geristration";
            this.button_geristration.Size = new System.Drawing.Size(350, 55);
            this.button_geristration.TabIndex = 5;
            this.button_geristration.Text = "Зареєструватися";
            this.button_geristration.UseVisualStyleBackColor = false;
            this.button_geristration.Click += new System.EventHandler(this.button_geristration_Click);
            // 
            // textBox_pass
            // 
            this.textBox_pass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_pass.BackColor = System.Drawing.Color.Honeydew;
            this.textBox_pass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_pass.Font = new System.Drawing.Font("Unispace", 40F, System.Drawing.FontStyle.Bold);
            this.textBox_pass.Location = new System.Drawing.Point(75, 214);
            this.textBox_pass.MaxLength = 15;
            this.textBox_pass.Name = "textBox_pass";
            this.textBox_pass.Size = new System.Drawing.Size(250, 64);
            this.textBox_pass.TabIndex = 4;
            this.textBox_pass.UseSystemPasswordChar = true;
            // 
            // pictureBox_lock
            // 
            this.pictureBox_lock.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox_lock.Image = global::Technopolis.Properties.Resources._lock;
            this.pictureBox_lock.Location = new System.Drawing.Point(4, 214);
            this.pictureBox_lock.Name = "pictureBox_lock";
            this.pictureBox_lock.Size = new System.Drawing.Size(65, 65);
            this.pictureBox_lock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_lock.TabIndex = 3;
            this.pictureBox_lock.TabStop = false;
            // 
            // textBox_login
            // 
            this.textBox_login.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_login.BackColor = System.Drawing.Color.Honeydew;
            this.textBox_login.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_login.Font = new System.Drawing.Font("Unispace", 40F, System.Drawing.FontStyle.Bold);
            this.textBox_login.Location = new System.Drawing.Point(150, 127);
            this.textBox_login.MaxLength = 15;
            this.textBox_login.Multiline = true;
            this.textBox_login.Name = "textBox_login";
            this.textBox_login.Size = new System.Drawing.Size(350, 65);
            this.textBox_login.TabIndex = 2;
            this.textBox_login.TabStop = false;
            this.textBox_login.Text = "Введіть логін";
            this.textBox_login.Click += new System.EventHandler(this.textBox_login_Click);
            // 
            // label_END_program
            // 
            this.label_END_program.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_END_program.Font = new System.Drawing.Font("Unispace", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_END_program.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.label_END_program.Location = new System.Drawing.Point(627, 0);
            this.label_END_program.Name = "label_END_program";
            this.label_END_program.Size = new System.Drawing.Size(23, 26);
            this.label_END_program.TabIndex = 1;
            this.label_END_program.Text = "x";
            this.label_END_program.Click += new System.EventHandler(this.label_END_program_Click);
            this.label_END_program.MouseEnter += new System.EventHandler(this.label_END_program_MouseEnter);
            this.label_END_program.MouseLeave += new System.EventHandler(this.label_END_program_MouseLeave);
            // 
            // pictureBox_login
            // 
            this.pictureBox_login.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox_login.Image = global::Technopolis.Properties.Resources.user;
            this.pictureBox_login.Location = new System.Drawing.Point(79, 127);
            this.pictureBox_login.Name = "pictureBox_login";
            this.pictureBox_login.Size = new System.Drawing.Size(65, 65);
            this.pictureBox_login.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_login.TabIndex = 1;
            this.pictureBox_login.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(195)))), ((int)(((byte)(0)))));
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(650, 71);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(70)))));
            this.label1.Font = new System.Drawing.Font("Unispace", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(24)))), ((int)(((byte)(69)))));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(650, 72);
            this.label1.TabIndex = 0;
            this.label1.Text = "Реєстрація";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 400);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RegisterForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RegisterForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_lock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_login)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_geristration;
        private System.Windows.Forms.TextBox textBox_pass;
        private System.Windows.Forms.PictureBox pictureBox_lock;
        private System.Windows.Forms.Label label_END_program;
        private System.Windows.Forms.PictureBox pictureBox_login;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_confirm_pass;
        private System.Windows.Forms.Label label_autorisation_window;
        private System.Windows.Forms.TextBox textBox_login;
    }
}