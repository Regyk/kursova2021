﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Technopolis
{
    
    public partial class MainForm : Form
    {
        static string type="", producer="", id_user="";
        static int i = 0;
        //static int summ = 0;
        public MainForm()
        {
            InitializeComponent();

            if(GlobalVars.loginUser=="qwer")
            {
                label1.Visible = false;
                label2.Visible = false;
                dataGrid_type.Visible = false;
                dataGrid_producer.Visible = false;
                dataGrid_product.Visible = false;
                label_refresh.Visible = false;
                dataGrid_order.Visible = false;
                label4.Visible = false;
                button_bye.Visible = false;
                dataGrid_users.Visible = true;
                textBox_order.Visible = true;
                textBox_product_add.Visible = true;
                comboBox_type_select.Visible = true;
                comboBox_produser_select.Visible = true;
                button_product_add.Visible = true;
                textBox_price.Visible = true;
            }
            

            DB db = new DB();

            DataTable table_producer = new DataTable();
            DataTable table_type = new DataTable();
            DataTable table_product = new DataTable();
            DataTable table_users = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command_producer = new MySqlCommand("SELECT `producer_name` FROM `producer` ", db.getConnection());
            MySqlCommand command_type = new MySqlCommand("SELECT `type` FROM `type` ", db.getConnection());
            MySqlCommand command_product = new MySqlCommand("SELECT `name` , `price`  FROM `product` ", db.getConnection());
            MySqlCommand command_users = new MySqlCommand("SELECT `id`, `login` , `position`  FROM `user` ", db.getConnection());

            MySqlCommand command_user_id = new MySqlCommand("SELECT `id` FROM `user` WERE login = @login", db.getConnection());
            command_user_id.Parameters.Add("@login", MySqlDbType.VarChar).Value = GlobalVars.loginUser;

            adapter.SelectCommand = command_user_id;
            //adapter.Fill(user_id);

            adapter.SelectCommand = command_producer;
            adapter.Fill(table_producer);
            dataGrid_producer.DataSource = table_producer;

            adapter.SelectCommand = command_type;
            adapter.Fill(table_type);
            dataGrid_type.DataSource = table_type;

            adapter.SelectCommand = command_product;
            adapter.Fill(table_product);
            dataGrid_product.DataSource = table_product;

            adapter.SelectCommand = command_users;
            adapter.Fill(table_users);
            dataGrid_users.DataSource = table_users;

            dataGrid_product.Columns[0].HeaderText = "Товар";
            dataGrid_product.Columns[1].HeaderText = "Ціна";
            dataGrid_product.Columns[1].Selected = false;
            dataGrid_users.Columns[0].HeaderText = "id";
            dataGrid_users.Columns[1].HeaderText = "login";
            dataGrid_users.Columns[2].HeaderText = "status";

        }

        private void label_END_program_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void label_END_program_MouseEnter(object sender, EventArgs e)
        {
            label_END_program.ForeColor = Color.Red;
        }

        private void label_END_program_MouseLeave(object sender, EventArgs e)
        {
            label_END_program.ForeColor = Color.FromArgb(88, 24, 69);
        }

        Point lastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void dataGrid_type_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            dataGrid_product.Columns[0].Width = 220;
            type = dataGrid_type.CurrentCell.Value.ToString();
            
            DB db = new DB();

            DataTable table_product = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            if (producer != "")
            {
                MySqlCommand command_product = new MySqlCommand("SELECT `name` , `price`  FROM `product` INNER JOIN `producer` ON product.id_producer = producer.id INNER JOIN `type` ON product.id_type = type.id WHERE producer_name = @producer AND type = @type", db.getConnection());

                command_product.Parameters.Add("@producer", MySqlDbType.VarChar).Value = producer;
                command_product.Parameters.Add("@type", MySqlDbType.VarChar).Value = type;

                adapter.SelectCommand = command_product;
                adapter.Fill(table_product);
                dataGrid_product.DataSource = table_product;
            }
            else
            {

                MySqlCommand command_product = new MySqlCommand("SELECT `name` , `price`  FROM `product` INNER JOIN `type` ON product.id_type = type.id WHERE type = @type ", db.getConnection());

                command_product.Parameters.Add("@type", MySqlDbType.VarChar).Value = type;

                adapter.SelectCommand = command_product;
                adapter.Fill(table_product);
                dataGrid_product.DataSource = table_product;
            }
            
        }

        private void label_refresh_MouseEnter(object sender, EventArgs e)
        {
            label_refresh.ForeColor = Color.Red;
        }

        private void label_refresh_MouseLeave(object sender, EventArgs e)
        {
            label_refresh.ForeColor = Color.FromArgb(88, 24, 69);
        }

        private void label_refresh_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm mainForm = new MainForm();
            mainForm.Show();
            i = 0;
        }

        private void dataGrid_producer_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGrid_product.Columns[0].Width = 220;
            producer = dataGrid_producer.CurrentCell.Value.ToString();

            DB db = new DB();

            DataTable table_product = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            if (type != "")
            {
                MySqlCommand command_product = new MySqlCommand("SELECT `name` , `price`  FROM `product` INNER JOIN `producer` ON product.id_producer = producer.id INNER JOIN `type` ON product.id_type = type.id WHERE producer_name = @producer AND type = @type", db.getConnection());

                command_product.Parameters.Add("@producer", MySqlDbType.VarChar).Value = producer;
                command_product.Parameters.Add("@type", MySqlDbType.VarChar).Value = type;

                adapter.SelectCommand = command_product;
                adapter.Fill(table_product);
                dataGrid_product.DataSource = table_product;
            }
            else
            {
                MySqlCommand command_product = new MySqlCommand("SELECT `name` , `price`  FROM `product` INNER JOIN `producer` ON product.id_producer = producer.id WHERE producer_name = @producer", db.getConnection());

                command_product.Parameters.Add("@producer", MySqlDbType.VarChar).Value = producer;

                adapter.SelectCommand = command_product;
                adapter.Fill(table_product);
                dataGrid_product.DataSource = table_product;
            }

            
        }

        

        private void dataGrid_product_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int summ = 0;
            int x=0, y;
            y = Convert.ToInt32(dataGrid_product.CurrentCell.RowIndex.ToString());
            dataGrid_order.ColumnCount = 4;
            dataGrid_order.RowCount = i;
            string value = dataGrid_product.Rows[y].Cells[x].Value.ToString();
            string count = dataGrid_product.Rows[y].Cells[x+1].Value.ToString();
            if (dataGrid_order.Rows.Count > 0 && dataGrid_order.Rows != null)
                {
                bool s = false;
                for (int k = 0; k < i; k++)
                {
                    
                    if (dataGrid_order.Rows[k].Cells[0].Value == dataGrid_product.Rows[y].Cells[0].Value)
                    {
                        s = true;
                    }
                }
                if(s == false)
                    {
                        dataGrid_order.Rows.Add(value, count, "1", "X");
                        i++;
                    for (int j = 0; j < i; j++)
                    { 
                        string p, c;
                        p = dataGrid_order.Rows[j].Cells[1].Value.ToString();
                        c = dataGrid_order.Rows[j].Cells[2].Value.ToString();
                        summ += Convert.ToInt32(p) * Convert.ToInt32(c);
                        label_summ.Text = summ.ToString();
                    }
                }
                
                }
            else
                {
                    i++;
                    dataGrid_order.Rows.Add(value, count, "1", "X");
                for (int j = 0; j < i; j++)
                {
                    string p, c;
                    p = dataGrid_order.Rows[j].Cells[1].Value.ToString();
                    c = dataGrid_order.Rows[j].Cells[2].Value.ToString();
                    summ += Convert.ToInt32(p) * Convert.ToInt32(c);
                    label_summ.Text = summ.ToString();
                }
            }
            dataGrid_order.Columns[0].HeaderText = "Товар";
            dataGrid_order.Columns[1].HeaderText = "Ціна";
            dataGrid_order.Columns[2].HeaderText = "кільк";
            dataGrid_order.Columns[0].ReadOnly = true;
            dataGrid_order.Columns[1].ReadOnly = true;
            dataGrid_order.Columns[3].ReadOnly = true;
        }

        private void dataGrid_users_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox_order.Text = "";
            int y = Convert.ToInt32(dataGrid_users.CurrentCell.RowIndex.ToString());
            string client_id = dataGrid_users.Rows[y].Cells[0].Value.ToString();

            DB db = new DB();

            MySqlCommand command_order = new MySqlCommand("SELECT `order_info` FROM `order_by` INNER JOIN `user` ON user.id = order_by.id_user WHERE id_user = @client_id", db.getConnection());

            command_order.Parameters.Add("@client_id", MySqlDbType.VarChar).Value = client_id;

            db.openConnection();

            //if (command_order.ExecuteNonQuery() == 1)
            //{
            //    try
            //{
            string order = command_order.ExecuteScalar().ToString();
            textBox_order.Text += ' ';
            for (int j = 0; j < order.Length; j++)
            {
                if (order[j].ToString() == "<")
                {
                    textBox_order.Text += " " + '\r' + '\n';
                }
                else
                {
                    textBox_order.Text += order[j].ToString();
                }
            }
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //}

            db.closeConnection();
        }

        private void button_unlogin_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
        }

        private void button_product_add_Click(object sender, EventArgs e)
        {
            int price = 0;
            DB db = new DB();
            DataTable table = new DataTable();
            DataTable table_user = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            if (textBox_product_add.Text == "")
            {
                MessageBox.Show("Назва не введена");
                return;
            }
            try
            {
                price = Convert.ToInt32(textBox_price.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("в ціну введено не число");
                return;
            }
            if (comboBox_type_select.SelectedIndex == -1)
            {
                MessageBox.Show("Тип не обрано");
                return;
            }
            if (comboBox_produser_select.SelectedIndex == -1)
            {
                MessageBox.Show("Виробника не обрано");
                return;
            }
            MySqlCommand command_check = new MySqlCommand("SELECT * FROM `product` WHERE `name` = @pN", db.getConnection());
            command_check.Parameters.Add("@pN", MySqlDbType.VarChar).Value = textBox_product_add.Text;

            adapter.SelectCommand = command_check;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Цей продукт вже існує");
                return;
            }
            else
            {
                MySqlCommand command_product_add = new MySqlCommand("INSERT INTO `product` (`name`, `price`, `id_type`, `id_producer`) VALUES (@name, @price, @id_type, @id_producer);", db.getConnection());

                command_product_add.Parameters.Add("@name", MySqlDbType.VarChar).Value = textBox_product_add.Text;
                command_product_add.Parameters.Add("@price", MySqlDbType.VarChar).Value = price;
                command_product_add.Parameters.Add("@id_type", MySqlDbType.VarChar).Value = comboBox_type_select.SelectedIndex+1;
                command_product_add.Parameters.Add("@id_producer", MySqlDbType.VarChar).Value = comboBox_produser_select.SelectedIndex + 1;

                db.openConnection();

                if (command_product_add.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("товар додано");
                }

                db.closeConnection();
            }

        }



        private void dataGrid_order_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int summ = 0;
            for (int j = 0; j < i; j++)
            {
                string p, c;
                p = dataGrid_order.Rows[j].Cells[1].Value.ToString();
                c = dataGrid_order.Rows[j].Cells[2].Value.ToString();
                try
                {
                    summ += Convert.ToInt32(p) * Convert.ToInt32(c);
                }
                catch (Exception ex)
                {
                    dataGrid_order.Rows[j].Cells[2].Value = "1";
                    MessageBox.Show("введіть число");
                }
                label_summ.Text = summ.ToString();
            }
            int x, y;
                x = Convert.ToInt32(dataGrid_order.CurrentCell.ColumnIndex.ToString());
                y = Convert.ToInt32(dataGrid_order.CurrentCell.RowIndex.ToString());
            if (x == 3)
            {
                string p, c;
                p = dataGrid_order.Rows[y].Cells[1].Value.ToString();
                c = dataGrid_order.Rows[y].Cells[2].Value.ToString();
                summ -= Convert.ToInt32(p) * Convert.ToInt32(c);
                label_summ.Text = summ.ToString();
                dataGrid_order.Rows.RemoveAt(y);
                dataGrid_order.Refresh();
                i--;
                
            }
            
        }
        private void button_bye_Click(object sender, EventArgs e)
        {
            
            for(int q = 0; q < dataGrid_users.Rows.Count; q++)
            {
                if (dataGrid_users.Rows[q].Cells[1].Value.ToString() == GlobalVars.loginUser)
                {
                    id_user = dataGrid_users.Rows[q].Cells[0].Value.ToString();
                }
            }
            if (dataGrid_order.Rows.Count > 0 && dataGrid_order.Rows != null) 
            { 
            int summ = 0;
            string order_info = "";
            for (int j = 0; j < i; j++)
            {
                string p, c;
                p = dataGrid_order.Rows[j].Cells[1].Value.ToString();
                c = dataGrid_order.Rows[j].Cells[2].Value.ToString();
                summ += Convert.ToInt32(p) * Convert.ToInt32(c);
                for (int k = 0; k < 3; k++)
                {
                    order_info += dataGrid_order.Rows[j].Cells[k].Value.ToString() + "  ";
                }
                order_info += "< \n";
            }
            order_info += "Сума = " + summ;
                DB db = new DB();
                MySqlCommand command_order = new MySqlCommand("UPDATE order_by SET order_info = @order_info WHERE id_user = @id_user", db.getConnection());
                command_order.Parameters.Add("@id_user", MySqlDbType.VarChar).Value = id_user;
                command_order.Parameters.Add("@order_info", MySqlDbType.VarChar).Value = order_info;

                db.openConnection();

                if (command_order.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Замовлення оформлено");
                }

                db.closeConnection();
            }
            else
            {
                MessageBox.Show("Корзина пуста");
            }
        }
    }
}
